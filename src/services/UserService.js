import axios from 'axios';
import UrlConstants from '../utils/UrlConstants';

class UserService {
    getUsers = () => {
        return axios.get(UrlConstants.USERS);
    }

    getUserById = (userId) => {
        return axios.get(UrlConstants.USER(userId));
    }

    getAddressesByUser = (userId) => {
        return axios.get(UrlConstants.BILLING_ADDRESSES(userId));
    }

    addAddress = (userId, address) => {
        return axios.post(UrlConstants.BILLING_ADDRESSES(userId), address);
    }

    updateAddress = (userId, addressId, address) => {
        return axios.put(UrlConstants.BILLING_ADDRESS(userId, addressId), address);
    }

    deleteAddress = (userId, addressId) => {
        return axios.delete(UrlConstants.BILLING_ADDRESS(userId, addressId));
    }

    getCardByUser = (userId) => {
        return axios.get(UrlConstants.CREDIT_CARDS(userId));
    }

    addCard = (userId, card) => {
        return axios.post(UrlConstants.CREDIT_CARDS(userId), card);
    }

    updateCard = (userId, cardId, card) => {
        return axios.put(UrlConstants.CREDIT_CARD(userId, cardId), card);
    }

    deleteCard = (userId, cardId) => {
        return axios.delete(UrlConstants.CREDIT_CARD(userId, cardId));
    }

    registerUser = (user) => {
        return axios.post(UrlConstants.USERS, user);
    }

    login = (user) => {
        return axios.post(UrlConstants.LOGIN, user);
    }

    updateUserDetails = (userId, user) => {
        return axios.delete(UrlConstants.USER(userId), user);
    }

    deleteUser = (userId) => {
        return axios.delete(UrlConstants.USER(userId));
    }
}

export default UserService;