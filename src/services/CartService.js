import axios from 'axios';
import UrlConstants from '../utils/UrlConstants';

class CartService {
    getCartItems = (userId) => {
        return axios.get(UrlConstants.CART_ITEMS(userId));
    }

    getCartItemById = (userId, id) => {
        return axios.get(UrlConstants.CART_ITEM(userId, id));
    }

    addItemToCart = (userId, cartItem) => {
        return axios.post(UrlConstants.CART_ITEMS(userId), cartItem);
    }

    removeCartItem = (userId, cartItemId) => {
        return axios.delete(UrlConstants.CART_ITEM(userId, cartItemId));
    }

    removeAllCartItems = (userId) => {
        return axios.delete(UrlConstants.CART_ITEMS(userId));
    }

    getTotalPrice = (cartItems) => {
        let totalPrice = 0;
        cartItems.map(item => {
            let product = item.productVariation.product;
            return totalPrice = totalPrice + ((parseFloat(product.price * ((100 - product.discountPercent)/100))) * item.quantity);
        })

        return totalPrice.toFixed(2);
    }
}

export default CartService;