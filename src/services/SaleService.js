import axios from 'axios';
import UrlConstants from '../utils/UrlConstants';

class SaleService {
    getOrders = () => {
        return axios.get(UrlConstants.SALES);
    }

    getOrdersByUser = (userId) => {
        return axios.get(UrlConstants.SALES + "?userId=" + userId);
    }

    getOrderById = (id) => {
        return axios.get(UrlConstants.SALE(id));
    }

    createSale = (sale) => {
        return axios.post(UrlConstants.SALES, sale);
    }
}

export default SaleService;