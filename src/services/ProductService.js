import axios from 'axios';
import UrlConstants from '../utils/UrlConstants';

class ProductService {
    getProducts = () => {
        return axios.get(UrlConstants.PRODUCTS);
    }

    getProductsByCategoryCodeAndType = (categoryCode, type) => {
        if(categoryCode != undefined) {
            return axios.get(UrlConstants.PRODUCTS + "?categoryCode=" + categoryCode.toUpperCase());
        } else if(type != undefined) {
            return axios.get(UrlConstants.PRODUCTS + "?type=" + type.toUpperCase());
        } else if(categoryCode !== undefined && type !== undefined) {
            return axios.get(UrlConstants.PRODUCTS + "?categoryCode=" + categoryCode.toUpperCase() + "&type=" + type.toUpperCase());
        } else {
            return axios.get(UrlConstants.PRODUCTS);
        }
    }

    getProductById = (id) => {
        return axios.get(UrlConstants.PRODUCT(id));
    }

    addProduct = (product) => {
        return axios.post(UrlConstants.PRODUCTS, product);
    }

    discountedPrice = (price, discountPercent) => {
        return parseFloat(price * ((100 - discountPercent)/100)).toFixed(2);
    }
}

export default ProductService;