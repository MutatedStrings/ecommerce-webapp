import axios from 'axios';
import UrlConstants from '../utils/UrlConstants';

class CategoryService {
    getCategories = () => {
        return axios.get(UrlConstants.CATEGORIES);
    }

    getCategory = (id) => {
        return axios.get(UrlConstants.CATEGORY(id));
    }

    addCategory = (category) => {
        return axios.post(UrlConstants.CATEGORIES, category);
    }

    disableCategory = (id) => {
        return axios.delete(UrlConstants.CATEGORY(id));
    }
}

export default CategoryService;