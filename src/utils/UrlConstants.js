class UrlConstants {
    static get SERVER_HOST() {
        return "http://localhost";
    }

    static get SERVER_PORT() {
        return ":8080";
    }

    static get API_ROUTE() {
        return "/api/v1";
    }

    static get BASE_URL() {
        return this.SERVER_HOST + this.SERVER_PORT + this.API_ROUTE;
    }

    // User api links
    static get USERS() {
        return this.BASE_URL + "/users";
    }
    static USER($id) {
        return this.USERS + "/" + $id;
    }

    static get LOGIN() {
        return this.USERS + "/login";
    }

    // Billing Address api links
    static BILLING_ADDRESSES($userId) {
        return this.USER($userId) + "/billingaddresses";
    }
    static BILLING_ADDRESS($userId, $id) {
        return this.BILLING_ADDRESSES($userId) + "/" + $id;
    }

    // Credit card api links
    static CREDIT_CARDS($userId) {
        return this.USER($userId)+ "/creditcards";
    }
    static CREDIT_CARD($userId, $id) {
        return this.CREDIT_CARDS($userId) + "/" + $id;
    }

    // Cart item api links
    static CART_ITEMS($userId) {
        return this.USER($userId) + "/cartitems";
    }
    static CART_ITEM($userId, $id) {
        return this.CART_ITEMS($userId) + "/" + $id;
    }

    // Product api links
    static get PRODUCTS() {
        return this.BASE_URL + "/products";
    }
    static PRODUCT($id) {
        return this.PRODUCTS + "/" + $id;
    }

    // Sale api links
    static get SALES() {
        return this.BASE_URL + "/orders";
    }
    static SALE($id) {
        return this.SALES + "/" + $id;
    }

    //Sale items api links
    static get SALE_ITEMS() {
        return this.BASE_URL + "/orderitems";
    }

    // User role api links
    static get USER_ROLES() {
        return this.BASE_URL + "/userroles";
    }
    static USER_ROLE($id) {
        return this.USER_ROLES + "/" + $id;
    }

    // Category api links
    static get CATEGORIES() {
        return this.BASE_URL + "/categories";
    }
    static CATEGORY($id) {
        return this.CATEGORIES + "/" + $id;
    }
}

export default UrlConstants;