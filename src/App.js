import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

// Components
import NavBar from './components/NavBar';
import ProductsGrid from './components/products/ProductsGrid';
import NotFound from "./components/errors/404";
import ProductDetails from "./components/products/ProductDetails";
import CartItemList from "./components/cart/CartItemList";
import Checkout from "./components/checkout/Checkout";
import AccountPage from "./components/users/AccountPage";
import Login from './components/users/Login';
import Dashboard from "./components/dashboard/Dashboard";
import AddProductForm from "./components/users/admin/AddProductForm";
import Register from "./components/users/Register";
import Home from './components/Home';
import {ADMIN, AuthRoute, CUSTOMER} from "./components/auth/AuthRoute";

function App() {
    document.title = "GiGi";
    return (
        <div>
            <NavBar />
            <div className={"container-fluid"}>
                <div className={"main py-4"}>
                    <Router>
                        <Switch>
                            <Route path={"/"} component={Home} exact/>
                            <Route path={"/products"} component={ProductsGrid} exact />
                            <Route path={"/products/:productId"} component={ProductDetails} exact />
                            <Route path={"/products/all/:type"} component={ProductsGrid} exact />
                            <Route path={"/products/:type/:categoryCode"} component={ProductsGrid} exact />
                            <AuthRoute path={"/cart"} component={CUSTOMER(CartItemList)} exact />
                            <AuthRoute path={"/checkout"} component={CUSTOMER(Checkout)} exact />
                            <AuthRoute path={"/account"} component={CUSTOMER(AccountPage)} exact />
                            <Route path={"/login"} component={Login} exact />
                            <Route path={"/register"} component={Register} exact />
                            <AuthRoute path={"/dashboard"} component={ADMIN(Dashboard)} exact />
                            <AuthRoute path={"/dashboard/products/add"} component={ADMIN(AddProductForm)} exact />
                            <Route component={NotFound} />
                        </Switch>
                    </Router>
                </div>
            </div>
        </div>
    )
}

export default App;
