import React, {Component} from 'react';
import UserService from "../../services/UserService";
import FormControl from "react-bootstrap/es/FormControl";
import Form from "react-bootstrap/Form";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class CardSelector extends Component {
    userService = new UserService();
    constructor(props) {
        super(props);

        this.state = {
            card: []
        }

    }

    componentDidMount() {
        this.getCards();
    }

    cardSelect = (event) => {
        let card = this.state.card.find(card =>
            card.cardNumber === event.target.value
        );

        this.props.fillCard(card);
    }

    render() {
        return (
            <div>
                <Form.Label>Available Card</Form.Label>
                <FormControl as={"select"} onChange={this.cardSelect}>
                    <option>Select card...</option>
                    {this.state.card.map(card => {
                        return <option key={card.cardId} value={card.cardNumber}>{card.cardNumber}</option>
                    })}
                </FormControl>
            </div>
        );
    }

    getCards = () => {
        this.userService.getCardByUser(getSessionValue(USER_ID))
            .then(res => this.setState({card: res.data}))
            .catch(error => console.error(error));
    }
}

export default CardSelector;