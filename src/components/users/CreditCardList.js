import React, {Component} from 'react';
import Table from "react-bootstrap/Table";
import UserService from "../../services/UserService";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus, faSave, faTrash} from "@fortawesome/free-solid-svg-icons";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class CreditCardList extends Component {
    userService = new UserService();

    constructor(props) {
        super(props);

        this.state = {
            cards: [],
            newCards: [],
        }
    }

    handleAddRow = () => {
        const card = {
            cardNumber: '',
            cardType: '',
            expirationMonth: '',
            expirationYear: '',
            holderName: ''
        };

        this.setState({
            newCards: [...this.state.newCards, card]
        });
    }

    handleDeleteCard = (id) => () => {
        const cards = [...this.state.cards];
        const card = cards[id];
        this.deleteCard(card.cardId);
        cards.splice(id, 1);
        this.setState({
            cards: cards
        })
    }

    handleRemoveRow = (id) => () => {
        const newCards = [...this.state.newCards];
        newCards.splice(id, 1);
        this.setState({
            newCards: newCards
        });
    }

    handleChange = (id) => (event) => {
        const {name, value} = event.target;
        const addresses = [...this.state.addresses];
        if (value !== "") {
            addresses[id] = {...this.state.addresses[id], [name]: value};
            this.updateCard(addresses[id]);
            this.setState({
                addresses: addresses
            });
        }
    }

    handleNewCardChange = (id) => (event) => {
        const {name, value} = event.target;
        const newCards = [...this.state.newCards];
        newCards[id] = {...this.state.newCards[id], [name]: value};
        this.setState({
            newCards: newCards
        });
    }

    handleSubmit = (event) => {
        const {newCards} = this.state;
        event.preventDefault();
        newCards.map(card => {
            return this.createCard(card);
        })
    }

    componentDidMount() {
        this.getCards();
    }

    render() {
        return (
            <div>
                <Form
                    onSubmit={this.handleSubmit}
                >
                    <ButtonToolbar>
                        <ButtonGroup>
                            <Button onClick={this.handleAddRow}><FontAwesomeIcon icon={faPlus}/> Add New</Button>
                            <Button variant={"success"} type={"submit"}><FontAwesomeIcon icon={faSave}/> Save
                                Cards</Button>
                        </ButtonGroup>
                    </ButtonToolbar><br/>
                    <Table>
                        <thead>
                        <tr>
                            <th>Card Number</th>
                            <th>Card Type</th>
                            <th>Expiration Month</th>
                            <th>Expiration Year</th>
                            <th>Holder Name</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.cards.map((card, index) => {
                            return (
                                <tr key={index}>
                                    <td><Form.Control onBlur={this.handleChange(index)} name={"cardNumber"}
                                                      defaultValue={card.cardNumber} required/></td>
                                    <td><Form.Control onBlur={this.handleChange(index)} name={"cardType"}
                                                      defaultValue={card.cardType} as={"select"} required>
                                        <option value={"Visa"}>Visa</option>
                                        <option value={"Mastercard"}>Mastercard</option>
                                    </Form.Control></td>
                                    <td><Form.Control onBlur={this.handleChange(index)} name={"expirationMonth"}
                                                      defaultValue={card.expirationMonth} type={"number"} min={0} max={12} required/></td>
                                    <td><Form.Control onBlur={this.handleChange(index)} name={"expirationYear"}
                                                      defaultValue={card.expirationYear} type={"number"} min={0} max={999} required/></td>
                                    <td><Form.Control onBlur={this.handleChange(index)} name={"holderName"}
                                                      defaultValue={card.holderName} required/></td>
                                    <td><Button variant={"danger"}
                                                onClick={this.handleDeleteCard(index)}><FontAwesomeIcon icon={faTrash}
                                                                                                        title={"Delete"}/></Button>
                                    </td>
                                </tr>);
                        })}
                        {this.state.newCards.map((item, index) => {
                            return (<tr key={index}>
                                <td><Form.Control name={"cardNumber"} value={this.state.newCards[index].cardNumber}
                                                  onChange={this.handleNewCardChange(index)} required/></td>
                                <td><Form.Control name={"cardType"} value={this.state.newCards[index].cardType}
                                                  onChange={this.handleNewCardChange(index)} as={"select"} required>
                                    <option value={"Visa"}>Visa</option>
                                    <option value={"Mastercard"}>Mastercard</option>
                                </Form.Control>
                                </td>
                                <td><Form.Control name={"expirationMonth"}
                                                  value={this.state.newCards[index].expirationMonth}
                                                  onChange={this.handleNewCardChange(index)} type={"number"} min={0} max={12} required/></td>
                                <td><Form.Control name={"expirationYear"} value={this.state.newCards[index].expirationYear}
                                                  onChange={this.handleNewCardChange(index)} type={"number"} min={0} max={999} required/></td>
                                <td><Form.Control name={"holderName"} value={this.state.newCards[index].holderName}
                                                  onChange={this.handleNewCardChange(index)} required/></td>
                                <td><Button variant={"danger"} onClick={this.handleRemoveRow(index)}><FontAwesomeIcon
                                    icon={faTrash} title={"Delete"}/></Button></td>
                            </tr>);
                        })}
                        </tbody>
                    </Table>
                </Form>
            </div>
        );
    }

    getCards = () => {
        this.userService.getCardByUser(getSessionValue(USER_ID))
            .then(res => this.setState({cards: res.data}))
            .catch(error => console.error(error));
    }

    createCard = (card) => {
        this.userService.addCard(getSessionValue(USER_ID), card)
            .then(res => alert("Successfully added cards"))
            .catch(error => console.error(error));
    }

    updateCard = (address) => {
        this.userService.updateCard(getSessionValue(USER_ID), address.addressId, address)
            .then(res => alert("Successfully updated"))
            .catch(error => console.error(error));
    }

    deleteCard = (cardId) => {
        this.userService.deleteCard(getSessionValue(USER_ID), cardId)
            .then(res => alert("Deleted card"))
            .catch(error => console.error(error));
    }
}

export default CreditCardList;