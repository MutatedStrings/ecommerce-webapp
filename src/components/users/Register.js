import React, {Component} from 'react';
import UserService from "../../services/UserService";
import logo from '../../logo.svg';
import Form from "react-bootstrap/Form";
import { AlertList } from "react-bs-notifier";

class Register extends Component {
    userService = new UserService();

    constructor() {
        super();

        this.state = {
            user: {},
            alerts: []
        }
    }

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState({user: {...this.state.user, [id]: value}});
    }

    handleSubmit = (event) => {
        let newUser = {...this.state.user};
        let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        let nameReg = /^[A-Z][a-z]{2,24}/;
        newUser.userRoleId = 'Cus';
        event.preventDefault();
        event.stopPropagation();
        console.log(newUser);
        let valid = true;
        if(newUser.password !== newUser.confirmPassword) {
            this.generateAlert("danger", "Something's not right!", "Passwords don't match");
            valid = false;
        }

        if(nameReg.test(newUser.firstName) === false || nameReg.test(newUser.lastName) === false) {
            this.generateAlert("danger", "Something's not right!", "Name should start with a capital letter and must contain only letters");
            valid = false;
        }

        if(newUser.password.length < 6) {
            this.generateAlert("danger", "Something's not right!", "Passwords don't match");
            valid = false;
        }

        if(emailReg.test(newUser.email) === false) {
            this.generateAlert("danger", "Something's not right!", "Not a valid email");
            valid = false;
        }
        if(valid === true) {
            this.registerUser(newUser);
            console.log(newUser);
        }
    }

    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;

        const idx = alerts.indexOf(alert);
        if(idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx+1)]});
        }
    }

    render() {
        const {user} = this.state;
        let alerts = this.renderAlerts();
        return (
            <div className={"container"} style={{textAlign: 'center'}}>
                {alerts}
                <Form className="form-signin" onSubmit={this.handleSubmit}>
                    <img className="mb-4" src={logo} alt=""
                         width="72" height="72"/>
                    <h6>Fill in the below details to create your account</h6>
                    <Form.Label htmlFor="username" className="sr-only">Username</Form.Label>
                    <Form.Control type="text" id="username" placeholder="Username"
                                  required onChange={this.handleChange} autoFocus/>
                    <Form.Label htmlFor="first_name" className="sr-only">First name</Form.Label>
                    <Form.Control type="text" id="firstName" placeholder="First name"
                                  required onChange={this.handleChange} autoFocus/>
                    <Form.Label htmlFor="last_name" className="sr-only">Last name</Form.Label>
                    <Form.Control type="text" id="lastName" placeholder="Last name"
                                  required onChange={this.handleChange} autoFocus/>
                    <Form.Label htmlFor="email" className="sr-only">Email</Form.Label>
                    <Form.Control type="email" id="email" placeholder="Email"
                                  required onChange={this.handleChange} autoFocus/>
                    <Form.Label htmlFor="password" className="sr-only">Password</Form.Label>
                    <Form.Control type="password" id="password" placeholder="Password"
                                  required onChange={this.handleChange}/>
                    <Form.Label htmlFor="confirmPassword" className="sr-only">Confirm Password</Form.Label>
                    <Form.Control type="password" id="confirmPassword" placeholder="Confirm password"
                                  required onChange={this.handleChange}/>
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Register</button>
                </Form>
            </div>
        );
    }

    registerUser = (user) => {
        this.userService.registerUser(user)
            .then(res => {
                this.setState({user: res.data});
                alert("Successfully registered!");
                window.location.href = "/";
            })
            .catch(error => console.error(error));
    }

    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }

        this.setState({alerts: [...this.state.alerts, newAlert]});
    }

    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed} alerts={this.state.alerts} />
        );
    }
}

export default Register;