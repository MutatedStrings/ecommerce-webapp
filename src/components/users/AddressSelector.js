import React, {Component} from 'react';
import UserService from "../../services/UserService";
import FormControl from "react-bootstrap/es/FormControl";
import Form from "react-bootstrap/Form";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class AddressSelector extends Component {
    userService = new UserService();
    constructor(props) {
        super(props);

        this.state = {
            addresses: []
        }
    }

    componentDidMount() {
        this.getAddresses();
    }

    addressSelect = (event) => {
        let address = this.state.addresses.find(address =>
            address.addressName === event.target.value
        );

        this.props.fillAddress(address);
    }

    render() {
        return (
            <div>
                <Form.Label>Available Addresses</Form.Label>
                <FormControl as={"select"} onChange={this.addressSelect}>
                    <option>Select address...</option>
                    {this.state.addresses.map(address => {
                        return <option key={address.addressId} value={address.addressName}>{address.addressName}</option>
                    })}
                </FormControl>
            </div>
        );
    }

    getAddresses = () => {
        this.userService.getAddressesByUser(getSessionValue(USER_ID))
            .then(res => this.setState({addresses: res.data}))
            .catch(error => console.error(error));
    }
}

export default AddressSelector;