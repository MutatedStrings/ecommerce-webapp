import React, {Component} from 'react';
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Slider from 'react-rangeslider';
import Image from "react-bootstrap/Image";
import CategoryService from "../../../services/CategoryService";
import {ClipLoader} from "react-spinners";
import AddVariationForm from "./AddVariationForm";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import ProductService from "../../../services/ProductService";

class AddProductForm extends Component {
    categoryService = new CategoryService();
    productService = new ProductService();

    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            product: {},
            formId: 0,
            variations: [],
            variationForms: [],
            isLoading: true
        }
    }

    componentDidMount() {
        document.title = "GiGi | Dashboard | Add Product";
        this.getCategories();
        this.addNewVariation();
    }

    addNewVariation = () => {
        const {variationForms, formId, variations} = this.state;
        this.setState({
            formId: formId + 1,
            variationForms: [...variationForms,
                <AddVariationForm key={formId} formId={formId} removeVariation={this.removeVariation}
                                  formUpdate={this.variationFormChange}/>]
        });
    }

    removeVariation = (key) => {
        const {variationForms, formId, variations} = this.state;
        console.log(key, variationForms);
        let newVariationForms = [...variationForms];
        if (newVariationForms.length > 1) {
            newVariationForms.splice(key, 1);
        }
        this.setState({variationForms: newVariationForms});
    }

    variationFormChange = (variation, formId) => {
        const {variations} = this.state;
        let newVariations = variations.slice(); // Copy the array
        newVariations[formId] = variation; // Edit variation details
        this.setState({variations: newVariations});
    }

    handleSubmit = () => {
        const {product, variations} = this.state;
        let newProduct = product;
        newProduct.productVariations = variations;
        if(newProduct.discountPercent === undefined) {
            newProduct.discountPercent = 0;
        }
        this.addNewProduct(newProduct);
        console.log(newProduct)
    }

    handleDiscountChange = (value) => {
        const {product} = this.state;
        this.setState({
            product: {...product, discountPercent: value}
        })
    };

    handleChange = (event) => {
        const {product} = this.state;
        this.setState({
            product: {...product, [event.target.id]: event.target.value}
        });
    }

    render() {
        const {categories, product, isLoading} = this.state;
        const horizontalLabels = {
            0: '0%',
            50: '50%',
            100: '100%'
        }
        const discountFormat = (value) => value + ' %'

        if (isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>);
        }

        return (
            <div>
                <Row>
                    <main role="main" className="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4">
                        <Form>
                            <div
                                className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                <h2>Add Product</h2>
                                <ButtonToolbar>
                                    <ButtonGroup>
                                        <Button variant={"success"} onClick={this.handleSubmit}>Add Product</Button>
                                        <Button variant={"warning"} onClick={this.addNewVariation}>Add New
                                            Variation</Button>
                                    </ButtonGroup>
                                </ButtonToolbar>
                            </div>

                            <Row>
                                <Col>
                                    <Form.Group as={Row} controlId="name">
                                        <Form.Label column sm={2}>
                                            Product Name
                                        </Form.Label>
                                        <Col sm={8}>
                                            <Form.Control type="text" onBlur={this.handleChange} required/>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="price">
                                        <Form.Label column sm={2}>
                                            Price
                                        </Form.Label>
                                        <Col sm={8}>
                                            <Form.Control type="number" step="any" onBlur={this.handleChange} required/>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="categoryCode">
                                        <Form.Label column sm={2}>
                                            Category
                                        </Form.Label>
                                        <Col sm={8}>
                                            <Form.Control as={"select"} onBlur={this.handleChange}>
                                                {categories.map((category, index) => {
                                                    return <option key={index}
                                                                   value={category.categoryCode}>{category.categoryCode + " - " + category.categoryName}</option>
                                                })}
                                            </Form.Control>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="discountPercent">
                                        <Form.Label column sm={2}>
                                            Discount Percentage
                                        </Form.Label>
                                        <Col sm={8}>
                                            <div className='slider'>
                                                <Slider
                                                    min={0}
                                                    max={100}
                                                    step={0.5}
                                                    value={product.discountPercent}
                                                    labels={horizontalLabels}
                                                    format={discountFormat}
                                                    onChange={this.handleDiscountChange}
                                                />
                                            </div>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="imageUrl">
                                        <Form.Label column sm={2}>
                                            Image Url
                                        </Form.Label>
                                        <Col sm={8}>
                                            <Form.Control type="text" onChange={this.handleChange} required/>
                                        </Col>
                                    </Form.Group>
                                    <br/>
                                </Col>
                                <Col align={"center"}>
                                    {product.imageUrl ? <Image
                                        src={product.imageUrl}
                                        title={product.productName}
                                        width={"30%"}
                                    /> : <div/>}
                                </Col>
                            </Row>
                            <br/>
                            <main role="main" className="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4">
                                <div
                                    className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <h3>Variations</h3>
                                </div>
                            </main>
                            {this.state.variationForms}
                        </Form>
                    </main>
                </Row>
            </div>
        );
    }

    getCategories = () => {
        this.categoryService.getCategories()
            .then(res => this.setState({categories: res.data, isLoading: false}))
            .catch(error => {
                console.error(error);
                this.setState({isLoading: false})
            });
    }

    addNewProduct = (product) => {
        this.productService.addProduct(product)
            .then(res => alert("Successfully added product"))
            .catch(error => console.error(error));
    }
}

export default AddProductForm;