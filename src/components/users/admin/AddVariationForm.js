import React, {Component} from 'react';
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";

class AddVariationForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            variation: {}
        }
    }

    componentDidMount() {
    }

    handleChange = (event) => {
        const {variation} = this.state;
        this.setState({
            variation: {...variation, [event.target.id]: event.target.value}
        }, () => this.props.formUpdate(this.state.variation, this.props.formId));
    }

    removeVariation = () => {
        this.props.removeVariation(this.props.formId);
    }

    render() {
        const {variation} = this.state;
        const {formId} = this.props;

        return (
            <React.Fragment>
                <br />
                <Row>
                    <Col>
                        <h6>Variation Details</h6>
                        <Form.Group as={Row} controlId="color">
                            <Form.Label column sm={2}>
                                Color
                            </Form.Label>
                            <Col sm={8}>
                                <Form.Control type="text" required onBlur={this.handleChange}/>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="size">
                            <Form.Label column sm={2}>
                                Size
                            </Form.Label>
                            <Col sm={8}>
                                <Form.Control type="text" onBlur={this.handleChange} required/>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="quantity">
                            <Form.Label column sm={2}>
                                Available Quantity
                            </Form.Label>
                            <Col sm={8}>
                                <Form.Control type="number" max={100} onBlur={this.handleChange} required/>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="imageUrl">
                            <Form.Label column sm={2}>
                                Image Url
                            </Form.Label>
                            <Col sm={8}>
                                <Form.Control type="text" onBlur={this.handleChange} required/>
                            </Col>
                        </Form.Group>
                        <br/>
                        <Button variant="danger" onClick={this.removeVariation}>Remove Variation</Button>
                    </Col>
                    <Col align={"center"}>
                        {variation.imageUrl ? <Image
                            src={variation.imageUrl}
                            title={variation.productName}
                            width={"30%"}
                        /> : <div/>}
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default AddVariationForm;