import React, {Component} from 'react';
import Table from "react-bootstrap/Table";
import UserService from "../../services/UserService";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus, faSave, faTrash} from "@fortawesome/free-solid-svg-icons";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class AddressList extends Component {
    userService = new UserService();

    constructor(props) {
        super(props);

        this.state = {
            addresses: [],
            newAddresses: [],
        }
    }

    handleAddRow = () => {
        const address = {
            addressName: '',
            addressLine1: '',
            addressLine2: '',
            street: '',
            city: '',
            country: ''
        };

        this.setState({
            newAddresses: [...this.state.newAddresses, address]
        });
    }

    handleDeleteAddress = (id) => () => {
        const addresses = [...this.state.addresses];
        const address = addresses[id];
        this.deleteAddress(address.addressId);
        addresses.splice(id, 1);
        this.setState({
            addresses: addresses
        })
    }

    handleRemoveRow = (id) => () => {
        const newAddresses = [...this.state.newAddresses];
        newAddresses.splice(id, 1);
        this.setState({
            newAddresses: newAddresses
        });
    }

    handleChange = (id) => (event) => {
        const {name, value} = event.target;
        const addresses = [...this.state.addresses];
        if(value !== "") {
            addresses[id] = {...this.state.addresses[id], [name]: value};
            this.updateAddress(addresses[id]);
            this.setState({
                addresses: addresses
            });
        }
    }

    handleNewAddressChange = (id) => (event) => {
        const {name, value} = event.target;
        const newAddresses = [...this.state.newAddresses];
        newAddresses[id] = {...this.state.newAddresses[id], [name]: value};
        this.setState({
            newAddresses: newAddresses
        });
    }

    handleSubmit = (event) => {
        const {newAddresses} = this.state;
        event.preventDefault();
        newAddresses.map(address => {
            return this.createAddress(address);
        })
    }

    componentDidMount() {
        this.getAddresses();
    }

    render() {
        return (
            <div>
                <Form
                    onSubmit={this.handleSubmit}
                >
                <ButtonToolbar>
                    <ButtonGroup>
                        <Button onClick={this.handleAddRow}><FontAwesomeIcon icon={faPlus}/> Add New</Button>
                        <Button variant={"success"} type={"submit"}><FontAwesomeIcon icon={faSave}/> Save
                            Addresses</Button>
                    </ButtonGroup>
                </ButtonToolbar><br/>
                <Table>
                    <thead>
                    <tr>
                        <th>Address Name</th>
                        <th>Address Line 1</th>
                        <th>Address Line 2</th>
                        <th>Street</th>
                        <th>City</th>
                        <th>Country</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.addresses.map((address, index) => {
                        return (
                            <tr key={index}>
                                <td><Form.Control onBlur={this.handleChange(index)} name={"addressName"} defaultValue={address.addressName} required/></td>
                                <td><Form.Control onBlur={this.handleChange(index)} name={"addressLine1"} defaultValue={address.addressLine1} required/></td>
                                <td><Form.Control onBlur={this.handleChange(index)} name={"addressLine2"} defaultValue={address.addressLine2} required/></td>
                                <td><Form.Control onBlur={this.handleChange(index)} name={"street"} defaultValue={address.street} required/></td>
                                <td><Form.Control onBlur={this.handleChange(index)} name={"city"} defaultValue={address.city} required/></td>
                                <td><Form.Control onBlur={this.handleChange(index)} name={"country"} defaultValue={address.country} required/></td>
                                <td><Button variant={"danger"} onClick={this.handleDeleteAddress(index)}><FontAwesomeIcon icon={faTrash} title={"Delete"}/></Button></td>
                            </tr>);
                    })}
                    {this.state.newAddresses.map((item, index) => {
                        return (<tr key={index}>
                            <td><Form.Control name={"addressName"} value={this.state.newAddresses[index].addressName} onChange={this.handleNewAddressChange(index)} required/></td>
                            <td><Form.Control name={"addressLine1"} value={this.state.newAddresses[index].addressLine1} onChange={this.handleNewAddressChange(index)} required/></td>
                            <td><Form.Control name={"addressLine2"} value={this.state.newAddresses[index].addressLine2} onChange={this.handleNewAddressChange(index)} required/></td>
                            <td><Form.Control name={"street"} value={this.state.newAddresses[index].street} onChange={this.handleNewAddressChange(index)} required/></td>
                            <td><Form.Control name={"city"} value={this.state.newAddresses[index].city} onChange={this.handleNewAddressChange(index)} required/></td>
                            <td><Form.Control name={"country"} value={this.state.newAddresses[index].country} onChange={this.handleNewAddressChange(index)} required/></td>
                            <td><Button variant={"danger"} onClick={this.handleRemoveRow(index)}><FontAwesomeIcon icon={faTrash} title={"Delete"}/></Button></td>
                        </tr>);
                    })}
                    </tbody>
                </Table>
                </Form>
            </div>
        );
    }

    getAddresses = () => {
        this.userService.getAddressesByUser(getSessionValue(USER_ID))
            .then(res => this.setState({addresses: res.data}))
            .catch(error => console.error(error));
    }

    createAddress = (address) => {
        this.userService.addAddress(getSessionValue(USER_ID), address)
            .then(res => alert("Successfully added addresses"))
            .catch(error => console.error(error));
    }

    updateAddress = (address) => {
        this.userService.updateAddress(getSessionValue(USER_ID), address.addressId, address)
            .then(res => alert("Successfully updated"))
            .catch(error => console.error(error));
    }

    deleteAddress = (addressId) => {
        this.userService.deleteAddress(getSessionValue(USER_ID), addressId)
            .then(res =>  alert("Deleted address"))
            .catch(error => console.error(error));
    }
}

export default AddressList;