import React, {Component} from 'react';
import UserService from "../../services/UserService";
import {ClipLoader} from "react-spinners";
import logo from '../../logo.svg';
import {Link} from "react-router-dom";
import Form from "react-bootstrap/Form";
import Image from "react-bootstrap/Image";
import Button from "react-bootstrap/Button";
import {AlertList} from "react-bs-notifier";
import {getRole, getSessionValue, setSessionValue} from "../auth/SessionManager";
import {EMAIL, NAME, USER_ID, USER_ROLE} from "../../utils/Constants";

class Login extends Component {
    userService = new UserService();

    constructor() {
        super();

        this.state = {
            user: {},
            alerts: []
        }
    }

    componentDidMount() {
        if(getSessionValue(USER_ID) !== '') {
            if(getRole() === 'Adm') {
                this.props.history.push("/dashboard");
            } else {
                this.props.history.push("/");
            }
        }
    }


    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState({user: {...this.state.user, [id]: value}});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.login();
    }

    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;

        const idx = alerts.indexOf(alert);
        if (idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)]});
        }
    }

    render() {
        const {user} = this.state;
        const alerts = this.renderAlerts();

        return (
            <div className={"container"} style={{textAlign: 'center'}}>
                {alerts}
                <Form className="form-signin" onSubmit={this.handleSubmit}>
                    <Image className="mb-4" src={logo} alt=""
                           width="72" height="72"/>
                    <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <Form.Label htmlFor="email" className="sr-only">Email address</Form.Label>
                    <Form.Control type="email" id="email" onChange={this.handleChange} className="form-control"
                                  placeholder="Email address"
                                  required autoFocus/>
                    <Form.Label htmlFor="password" className="sr-only">Password</Form.Label>
                    <Form.Control type="password" id="password" onChange={this.handleChange} className="form-control"
                                  placeholder="Password"
                                  required/>
                    <Button variant="primary" block type="submit">Sign in</Button>
                </Form>
                <Link to={"/register"}>Don't have an account? Click here to register.</Link>
            </div>
        );
    }

    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }

        this.setState({alerts: [...this.state.alerts, newAlert]});
    }

    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed}
                       alerts={this.state.alerts}/>
        );
    }

    login = () => {
        this.userService.login(this.state.user)
            .then(res => {
                this.setState({user: res.data}, () => this.setSessionValues(this.state.user.userId,
                    this.state.user.firstName, this.state.user.lastName, this.state.user.email, this.state.user.userRole.roleId));
                if(getRole() === 'Adm') {
                    window.location.href = "/dashboard";
                } else {
                    window.location.href = "/";
                }
            })
            .catch(error => {
                console.error(error);
                if (error.response !== undefined) {
                    if (error.response.status === 403) {
                        this.generateAlert("danger", "Something's not right!", "Invalid credentials");
                    }
                }
            });
    }

    setSessionValues = (userId, firstName, lastName, email, userRole) => {
        setSessionValue(USER_ID, userId);
        setSessionValue(NAME, firstName + " " + lastName);
        setSessionValue(EMAIL, email);
        setSessionValue(USER_ROLE, userRole);

    }
}

export default Login;