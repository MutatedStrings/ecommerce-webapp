import React, {Component} from 'react';
import SaleService from "../../services/SaleService";
import Table from "react-bootstrap/Table";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class SalesList extends Component {
    salesService = new SaleService();

    constructor(props) {
        super(props);

        this.state = {
            orders: []
        }
    }

    componentDidMount() {
        this.getOrdersByUser();
    }

    render() {
        return (
            <div>
                <Table hover>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Order Date</th>
                        <th>Order Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.orders.map((order, index) => {
                        return (
                            <tr key={index}>
                                <td>{order.saleId}</td>
                                <td>{order.orderDate}</td>
                                <td>LKR {order.totalAmount.toFixed(2)}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>
        );
    }

    getOrdersByUser = () => {
        this.salesService.getOrdersByUser(getSessionValue(USER_ID))
            .then(res => this.setState({orders: res.data}))
            .catch(error => console.error(error));
    }
}

export default SalesList;