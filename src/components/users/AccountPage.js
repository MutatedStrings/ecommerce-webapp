import React, {Component} from 'react';
import UserService from "../../services/UserService";
import Row from "react-bootstrap/Row";
import {ClipLoader} from "react-spinners";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import AddressList from "./AddressList";
import CreditCardList from "./CreditCardList";
import SalesList from "./SalesList";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class AccountPage extends Component {
    userService = new UserService();

    constructor() {
        super();

        this.state = {
            user: {},
            isLoading: true
        }
    }

    componentDidMount() {
        this.getUser();
    }

    render() {
        const {user, isLoading} = this.state;
        if (isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            )
        }

        return (
            <Container>
                <Card>
                    <Card.Header>My Account</Card.Header>
                    <Card.Body>
                        <Card.Title><Row><Col md={8}>{user.firstName + " " + user.lastName}</Col><Col>
                        </Col></Row></Card.Title>
                        <Card.Body>
                            <Row><Col md={2}>Username: </Col><Col>{user.username}</Col></Row>
                            <Row><Col md={2}>First Name: </Col><Col>{user.firstName}</Col></Row>
                            <Row><Col md={2}>Last Name: </Col><Col>{user.lastName}</Col></Row>
                            <Row><Col md={2}>Email:</Col><Col>{user.email}</Col></Row>
                            <Row><Col md={2}>Role: </Col><Col>{user.userRole.roleTitle}</Col></Row>
                        </Card.Body>

                        <Tabs id="uncontrolled-tab-example">
                            <Tab eventKey="home" title="Billing Addresses">
                                <div><br/><AddressList/></div>
                            </Tab>
                            <Tab eventKey="profile" title="Credit Card">
                                <div><br/><CreditCardList/></div>
                            </Tab>
                            <Tab eventKey="contact" title="Previous Orders">
                                <div><br/><SalesList/></div>
                            </Tab>
                        </Tabs>
                    </Card.Body>
                </Card>
            </Container>
        );
    }

    getUser = () => {
        this.userService.getUserById(getSessionValue(USER_ID))
            .then(res => this.setState({user: res.data, isLoading: false}))
            .catch(error => console.error(error));
    }
}

export default AccountPage;