import {EMAIL, NAME, USER_ROLE, USER_ID} from "../../utils/Constants";

export const setSessionValue = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
};

export const getSessionValue = (key) => {
    let value = localStorage.getItem(key);
    if (value === '') {
        return value
    }
    return JSON.parse(localStorage.getItem(key));
};

export const getRole = () => {
    let role = localStorage.getItem(USER_ROLE);
    if (!role) {
        return '';
    }
    return JSON.parse(role);
};

export const deleteSession = () => {
    localStorage.setItem(USER_ID, '');
    localStorage.setItem(USER_ROLE, '');
    localStorage.setItem(EMAIL, '');
    localStorage.setItem(NAME, '');
};