import React from 'react';
import {getRole, getSessionValue} from "./SessionManager";
import {Redirect, Route} from "react-router-dom";
import Alert from "react-bootstrap/Alert";
import {USER_ID} from "../../utils/Constants";

const checkAuth = () => {
    return getSessionValue(USER_ID);
}

export const AuthRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        checkAuth() ? (
            <Component {...props} />
        ) : (
            <Redirect to={{ pathname: '/login' }} />
        )
    )} />
);

export const Authorization = (allowedRoles) => (WrappedComponent) => {
    return class WithAuthorization extends React.Component {
        render() {
            if (allowedRoles.some(role => role === getRole())) {
                return <WrappedComponent {...this.props} />
            } else {
                return (<Alert variant={"warning"}>"You are not authorized to view this page"</Alert>);
            }
        }
    };
};

export const ADMIN = Authorization(['Adm']);
export const CUSTOMER = Authorization(['Adm', 'Cus']);