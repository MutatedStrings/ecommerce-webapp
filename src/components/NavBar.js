import React, {Component} from 'react';
import {Navbar, Nav, NavDropdown} from 'react-bootstrap';
import logo from '../logo.svg';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingBag} from '@fortawesome/free-solid-svg-icons';
import CategoryService from "../services/CategoryService";
import {deleteSession, getSessionValue} from "./auth/SessionManager";
import {NAME, USER_ID} from "../utils/Constants";

class NavBar extends Component {
    categoryService = new CategoryService();

    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            menCategories: [],
            womenCategories: []
        }
    }

    componentDidMount() {
        this.getCategories();
    }

    handleLogout = (event) => {
        deleteSession();
        window.location.href = "/";
    }

    render() {
        return (
            <Navbar bg="primary" expand="lg" variant="dark">
                <Navbar.Brand href="/">
                    <img
                        alt=""
                        src={logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />
                    {' GiGi '}
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="/products">Products</Nav.Link>
                        <NavDropdown title="Womens" id="basic-nav-dropdown">
                            {this.state.womenCategories.map((category, index) =>
                                <NavDropdown.Item key={index}
                                                  href={"/products/women/" + category.categoryCode}>{category.categoryName}</NavDropdown.Item>
                            )}
                            <NavDropdown.Divider/>
                            <NavDropdown.Item href="/products/all/women">All Women's</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Mens" id="basic-nav-dropdown">
                            {this.state.menCategories.map((category, index) =>
                                <NavDropdown.Item key={index}
                                                  href={"/products/men/" + category.categoryCode}>{category.categoryName}</NavDropdown.Item>
                            )}
                            <NavDropdown.Divider/>
                            <NavDropdown.Item href="/products/all/men">All Men's</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Nav>
                        <Nav.Link href="/cart"><FontAwesomeIcon icon={faShoppingBag} title={"View Bag"}
                                                                color={"white"}/></Nav.Link>
                        {getSessionValue(USER_ID) === '' ? (<Nav.Link href="/login">Login</Nav.Link>) :
                            (
                                <NavDropdown alignRight title={getSessionValue(NAME)} id="basic-nav-dropdown">
                                    <NavDropdown.Item href={"/account"}>Account</NavDropdown.Item>
                                    <NavDropdown.Divider/>
                                    <NavDropdown.Item onClick={this.handleLogout}>Logout</NavDropdown.Item>
                                </NavDropdown>
                            )
                        }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }

    getCategories = () => {
        this.categoryService.getCategories()
            .then(res => this.setState({categories: res.data}, () => {
                this.getMenCategories();
                this.getWomenCategories();
            }))
            .catch(error => console.log(error));
    }

    getMenCategories = () => {
        this.setState({menCategories: this.state.categories.filter(category => category.type === "MEN")});
    }

    getWomenCategories = () => {
        this.setState({womenCategories: this.state.categories.filter(category => category.type === "WOMEN")});
    }
};

export default NavBar;