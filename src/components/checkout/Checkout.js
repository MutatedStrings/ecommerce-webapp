import React, {Component} from 'react';
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import CartSideBar from "../cart/CartSideBar";
import CartService from "../../services/CartService";
import AddressSelector from '../users/AddressSelector';
import {ClipLoader} from "react-spinners";
import logo from '../../logo.svg';
import SaleService from "../../services/SaleService";
import Container from "react-bootstrap/Container";
import CardSelector from "../users/CardSelector";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class Checkout extends Component {
    cartService = new CartService();
    saleService = new SaleService();

    constructor() {
        super();

        this.state = {
            cartItems: [],
            sale: {},
            isLoading: true,
            validated: false
        }
    }

    componentDidMount() {
        const {sale} = this.state;
        this.getCartItems();
        this.setState({
            sale: {
                ...sale,
                addressLine1: '',
                addressLine2: '',
                street: '',
                city: '',
                country: '',
                cardNumber: '',
                holderName: '',
                expiryDate: ''
            }
        });
    }

    handleChange = (event) => {
        const {sale} = this.state;
        this.setState({
            sale: {...sale, [event.target.id]: event.target.value}
        });
    }

    handleSubmit = (event) => {
        const form = event.currentTarget;
        const {sale, cartItems} = this.state;
        if (cartItems.length === 0) {
            alert("No items in cart to checkout.");
            event.preventDefault();
            event.stopPropagation();
        } else if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            event.preventDefault();
            sale.orderDate = new Date();
            sale.totalAmount = this.cartService.getTotalPrice(cartItems);
            sale.saleItems = cartItems.map(item => {
                let saleItem = {};
                saleItem.qty = item.quantity;
                saleItem.pricePerUnit = item.productVariation.product.price;
                saleItem.productVariationId = item.productVariation.variationId;

                return saleItem;
            });
            this.checkout();
            this.props.history.push("/products");
        }
        this.setState({validated: true});
    }

    handleAddressChange = (address) => {
        if (address !== undefined) {
            this.setState({
                sale: {
                    ...this.state.sale,
                    addressLine1: address.addressLine1,
                    addressLine2: address.addressLine2,
                    street: address.street,
                    city: address.city,
                    country: address.country
                }
            });
        } else {
            this.setState({
                sale: {
                    ...this.state.sale,
                    firstName: "",
                    lastName: "",
                    addressLine1: "",
                    addressLine2: "",
                    street: "",
                    city: "",
                    country: ""
                }
            })
        }
    }

    handleCardChange = (card) => {
        if (card !== undefined) {
            this.setState({
                sale: {
                    ...this.state.sale,
                    cardNumber: card.cardNumber,
                    holderName: card.holderName,
                    expiryDate: card.expirationMonth + "/" + card.expirationYear,
                    cardType: card.cardType
                }
            });
        } else {
            this.setState({
                sale: {
                    ...this.state.sale,
                    cardNumber: "",
                    holderName: "",
                    expiryDate: "",
                    cardType: ""
                }
            })
        }
    }

    render() {
        const {isLoading, cartItems, validated, sale} = this.state;
        if (isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            )
        }

        return (
            <Container>
                <div className="py-5 text-center">
                    <Image className="d-block mx-auto mb-4"
                           src={logo}
                           alt="" width="72" height="72"/>
                    <h2>Checkout form</h2>
                    <p className="lead">You are almost done with your order. Just fill in the below details and make
                        the payment and it's all yours.</p>
                </div>

                <Row>
                    <CartSideBar cartItems={cartItems}/>
                    <Col md={7} className="order-md-1">
                        <h4 className="mb-3">Billing address</h4>
                        <Form className="needs-validation"
                              noValidate
                              validated={validated}
                              onSubmit={this.handleSubmit}
                        >
                            <Row>
                                <Col md={6} className="mb-3">
                                    <Form.Label htmlFor="firstName">First name</Form.Label>
                                    <Form.Control type="text" id="firstName" placeholder="John" defaultValue=""
                                                  onChange={this.handleChange} required/>
                                    <div className="invalid-feedback">
                                        Valid first name is required.
                                    </div>
                                </Col>
                                <Col md={6} className="mb-3">
                                    <Form.Label htmlFor="lastName">Last name</Form.Label>
                                    <Form.Control type="text" id="lastName" placeholder="Doe" defaultValue=""
                                                  onChange={this.handleChange} required/>
                                    <div className="invalid-feedback">
                                        Valid last name is required.
                                    </div>
                                </Col>
                            </Row>

                            {/*Billing Address*/}
                            <div className="mb-3">
                                <AddressSelector fillAddress={this.handleAddressChange}/>
                                <Form.Label htmlFor="address">Address Line 1</Form.Label>
                                <Form.Control type="text" id="addressLine1" placeholder="No 1234"
                                              onChange={this.handleChange}
                                              value={sale.addressLine1 !== "" ? sale.addressLine1 : ""} required/>
                                <div className="invalid-feedback">
                                    Please enter your delivery address.
                                </div>
                            </div>

                            <div className="mb-3">
                                <Form.Label htmlFor="address2">Address Line 2</Form.Label>
                                <Form.Control type="text" id="addressLine2"
                                              onChange={this.handleChange}
                                              value={sale.addressLine2 !== "" ? sale.addressLine2 : ""}
                                              placeholder="Wijerama Mawatha" required/>
                            </div>

                            <Row>
                                <Col md={4} mb={3}>
                                    <Form.Label htmlFor="street">Street</Form.Label>
                                    <Form.Control type="text" id="street" placeholder="Hill Street"
                                                  value={sale.street !== "" ? sale.street : ""}
                                                  onChange={this.handleChange} required/>
                                    <div className="invalid-feedback">
                                        Street required.
                                    </div>
                                </Col>
                                <Col md={4} mb={3}>
                                    <Form.Label htmlFor="city">City</Form.Label>
                                    <select className="custom-select d-block w-100" id="city"
                                            onChange={this.handleChange}
                                            value={sale.city !== "" ? sale.city : ""} required>
                                        <option value="">Choose...</option>
                                        <option>Colombo</option>
                                    </select>
                                    <div className="invalid-feedback">
                                        Please provide a valid city.
                                    </div>
                                </Col>
                                <Col md={4} mb={3}>
                                    <Form.Label htmlFor="country">Country</Form.Label>
                                    <select className="custom-select d-block w-100" id="country"
                                            value={sale.country !== "" ? sale.country : ""}
                                            onChange={this.handleChange} required>
                                        <option value="">Choose...</option>
                                        <option>Sri Lanka</option>
                                    </select>
                                    <div className="invalid-feedback">
                                        Please select a valid country.
                                    </div>
                                </Col>
                            </Row>
                            <hr className="mb-4"/>

                            <h4 className="mb-3">Payment</h4>

                            <div className="d-block my-3">
                                <div className="custom-control custom-radio">
                                    <Form.Control id="visa" name="paymentMethod" type="radio"
                                                  className="custom-control-input" defaultChecked required/>
                                    <Form.Label className="custom-control-label" htmlFor="visa">Visa</Form.Label>
                                </div>
                                <div className="custom-control custom-radio">
                                    <Form.Control id="mastercard" name="paymentMethod" type="radio"
                                                  className="custom-control-input" required/>
                                    <Form.Label className="custom-control-label"
                                                htmlFor="mastercard">Mastercard</Form.Label>
                                </div>
                            </div>

                            {/*Credit card*/}
                            <Row>
                                <Col><CardSelector fillCard={this.handleCardChange}/></Col>
                            </Row>
                            <Row>
                                <Col md={6} mb={3}>
                                    <Form.Label htmlFor="holderName">Name on card</Form.Label>
                                    <Form.Control type="text" id="holderName" placeholder=""
                                                  onChange={this.handleChange} value={sale.holderName} required/>
                                    <small className="text-muted">Full name as displayed on card</small>
                                    <div className="invalid-feedback">
                                        Name on card is required
                                    </div>
                                </Col>
                                <Col md={6} mb={3}>
                                    <Form.Label htmlFor="cardNumber">Credit card number</Form.Label>
                                    <Form.Control type="text" id="cardNumber" placeholder=""
                                                  onChange={this.handleChange} value={sale.cardNumber} required/>
                                    <div className="invalid-feedback">
                                        Credit card number is required
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={3} mb={3}>
                                    <Form.Label htmlFor="expiryDate">Expiration</Form.Label>
                                    <Form.Control type="text" id="expiryDate" placeholder=""
                                                  onChange={this.handleChange} value={sale.expiryDate} required/>
                                    <div className="invalid-feedback">
                                        Expiration date required
                                    </div>
                                </Col>
                                <Col md={3} mb={3}>
                                    <Form.Label htmlFor="cc-expiration">CVV</Form.Label>
                                    <Form.Control type="password" id="cc-cvv" placeholder=""
                                                  required/>
                                    <div className="invalid-feedback">
                                        Security code required
                                    </div>
                                </Col>
                            </Row>
                            <hr className="mb-4"/>
                            <Button variant={"primary"} size={"lg"} block type="submit">Continue to
                                checkout
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }

    getCartItems = () => {
        this.cartService.getCartItems(getSessionValue(USER_ID))
            .then(res => {
                this.setState({cartItems: res.data, isLoading: false})
            })
            .catch(error => {
                console.error(error);
                this.setState({isLoading: false});
            });
    }

    checkout = () => {
        let sale = this.state.sale;
        sale.userId = getSessionValue(USER_ID);
        this.saleService.createSale(sale)
            .then(res => alert("Successful checkout"))
            .catch(error => {
                console.error(error);
            });
    }
}

export default Checkout;