import React from 'react';
import {Link} from 'react-router-dom';
import Button from "react-bootstrap/Button";

const NotFound = () => {
    document.title = "404 Not Found";

    return (
        <div className={"App-header"}>
            <div>
                <div>
                    <h1>Error 404</h1>
                </div>
                <p>The page you are looking for might have been removed had its name changed or is temporarily
                    unavailable.</p>
                <Link to={'/'}><Button>Back to homepage</Button></Link>
            </div>
        </div>
    )
}

export default NotFound;