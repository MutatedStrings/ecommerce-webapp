import React from 'react';
import Image from "react-bootstrap/Image";
import {Link} from "react-router-dom";

const Home = () => {
    return (
        <React.Fragment>
            <Link to={"/products/all/women"}>
                <Image width={"100%"}
                       src="https://www.boohoo.com/on/demandware.static/-/Library-Sites-boohoo-content-global/default/dwac3b89da/images/home/goingouteditdesk110619UK.jpg"/>
            </Link>
            <br/><br/>
            <Link to={"/products/all/men"}>
                <Image width={"100%"}
                       src="https://www.boohoo.com/on/demandware.static/-/Library-Sites-boohoo-content-global/default/dwa6ced4b1/images/home/1706_UK_MENS_PRIDE_DESK.jpg"/>
            </Link>
        </React.Fragment>
    )
}

export default Home;