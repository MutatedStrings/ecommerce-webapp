import React, {Component} from 'react';
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import CardColumns from "react-bootstrap/CardColumns";
import {Link} from "react-router-dom";

class Dashboard extends Component {

    render() {
        return (
            <React.Fragment>
                <div className="container-fluid">
                    <Row>
                        <main role="main" className="col-md-9 ml-sm-auto col-lg-12 pt-3 px-4">
                            <div
                                className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                <h2>Dashboard</h2>
                            </div>
                            <CardColumns>
                                <Link to={"/dashboard/products/add"}>
                                    <Card>
                                        <Card.Header>Product</Card.Header>
                                        <Card.Body>
                                            <Card.Title>Add Product</Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">Create</Card.Subtitle>
                                            <Card.Text>
                                                Click here to add more products to the product catalogue.
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Link>
                                <Link to={"/dashboard/products/update"}>
                                    <Card>
                                        <Card.Header>Product</Card.Header>
                                        <Card.Body>
                                            <Card.Title>Update Product</Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">Edit</Card.Subtitle>
                                            <Card.Text>
                                                Select one of the actions below to proceed product operations.
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Link>
                                <Link to={"/dashboard/categories/add"}>
                                    <Card>
                                        <Card.Header>Category</Card.Header>
                                        <Card.Body>
                                            <Card.Title>Add Category</Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">Add</Card.Subtitle>
                                            <Card.Text>
                                                Click here to view all available categories and to add/remove
                                                categories.
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Link>
                            </CardColumns>
                        </main>
                    </Row>
                </div>
            </React.Fragment>
        );
    }
}

export default Dashboard;