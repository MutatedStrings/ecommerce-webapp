import React, {Component} from 'react';
import {ClipLoader} from 'react-spinners';
import Alert from 'react-bootstrap/Alert';
import ProductService from "../../services/ProductService";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMinus, faPlus} from '@fortawesome/free-solid-svg-icons';
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {FormControl} from "react-bootstrap";
import { AlertList } from "react-bs-notifier";
import CartService from "../../services/CartService";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class ProductDetails extends Component {
    productService = new ProductService();
    cartService = new CartService();

    constructor() {
        super();

        this.state = {
            image: '',
            product: {},
            productVariations: [],
            colors: [],
            sizes: [],
            selectedColor: '',
            selectedSize: '',
            selectedVariation: {},
            qty: 1,
            isLoading: true,
            errors: [],
            alerts: []
        }
    }

    componentDidMount() {
        this.getProduct();
    }

    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;

        const idx = alerts.indexOf(alert);
        if(idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx+1)]});
        }
    }

    handleColorChange = (event) => {
        this.setState({selectedColor: event.target.value}, () => this.getSizes());
    }

    handleSizeChange = (event) => {
        let selectedVariation = this.getVariationByColorAndSize(this.state.selectedColor, event.target.value);
        this.setState({
            selectedSize: event.target.value,
            selectedVariation: selectedVariation,
            image: selectedVariation.imageUrl,
            qty: 1
        });
    }

    handleAddQty = (event) => {
        let {selectedVariation, qty} = this.state;
        let newQty = qty;
        if (qty < selectedVariation.quantity) {
            newQty = qty + 1;
        }
        this.setState({qty: newQty});
    }

    handleReduceQty = (event) => {
        let {qty} = this.state;
        let newQty = qty;
        if (qty > 1) {
            newQty = qty - 1;
        }
        this.setState({qty: newQty});
    }

    render() {
        const {image, product, isLoading, errors, colors, sizes, qty} = this.state;
        let price = parseFloat(product.price).toFixed(2);

        if (isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>);
        }

        if (errors.length !== 0) {
            return (<React.Fragment>{this.renderErrors()}</React.Fragment>);
        }

        let priceDisplay = (<h3 className="product-price">LKR {price}</h3>);

        if (product.discountPercent > 0) {
            priceDisplay = (<h3 className="product-price">LKR {this.productService.discountedPrice(price,
                product.discountPercent)} ({product.discountPercent}% off) <strike>LKR {price}</strike></h3>);
        }

        return (
            <div className="container" id="product-section">
                <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed} alerts={this.state.alerts} />
                <Row>
                    <Col md={6}>
                        <Image
                            src={image}
                            alt={product.name}
                            className="image-responsive"
                        />
                    </Col>
                    <Col md={6}>
                        <Row>
                            <Col md={12}>
                                <h2>{product.name}</h2>
                                <Row>
                                    <Col md={12}>
                                        <Badge variant={"primary"}>{product.category.categoryName}</Badge>
                                        <span>Product No. {product.productId}</span>
                                    </Col>
                                </Row>
                                <hr/>
                                <Row>
                                    <Col md={12} bottom={"rule"}>
                                        {priceDisplay}
                                    </Col>
                                </Row>
                                <hr/>
                                <Row>
                                    <Col md={12}>
                                        <FormControl as={"select"} onChange={this.handleColorChange}>
                                            {colors.map((color, index) => {
                                                return <option key={index} value={color}>{color}</option>
                                            })}
                                        </FormControl>
                                    </Col>
                                </Row>
                                <br/>
                                <Row>
                                    <Col md={12}>
                                        <FormControl as={"select"} onChange={this.handleSizeChange} multiple>
                                            {sizes.map((size, index) => {
                                                return <option key={index}>{size}</option>
                                            })}
                                        </FormControl>
                                    </Col>
                                </Row>
                                <br/>
                                <Row className="add-to-cart">
                                    <Col md={4} className="product-qty">
                                        <Button onClick={this.handleAddQty}>
                                            <FontAwesomeIcon icon={faPlus}/>
                                        </Button>
                                        <Form.Label>{qty}</Form.Label>
                                        <Button onClick={this.handleReduceQty}>
                                            <FontAwesomeIcon icon={faMinus}/>
                                        </Button>
                                    </Col>
                                    <Col md={6}>
                                        <Button onClick={this.addToCart}>
                                            Add to Bag
                                        </Button>
                                    </Col>
                                </Row>
                                <hr/>
                                <Row>
                                    <div className="col-md-12 top-10">
                                        <p>To order by telephone, <a href="tel:0112729729">please call
                                            0112-729-729</a></p>
                                    </div>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }

    getProduct = () => {
        let productId = this.props.match.params.productId;
        this.productService.getProductById(productId)
            .then(res => {
                this.setState({
                    product: res.data,
                    productVariations: res.data.productVariations,
                    isLoading: false,
                    image: res.data.imageUrl
                }, () => this.getColors());
                document.title = "GiGi | " + this.state.product.name;
            })
            .catch(error => {
                console.error(error);
                if(error.response !== undefined) {
                    if (error.response.status === 404) {
                        const message = 'No such product found!';
                        this.setState({errors: [...this.state.errors, message], isLoading: false});
                    }
                } else {
                    console.error(error);
                }
            })
    }


    addToCart = () => {
        const {qty, selectedVariation} = this.state;
        let cartItem = {};
        cartItem.productVariationId = selectedVariation.variationId;
        cartItem.quantity = qty;

        this.cartService.addItemToCart(getSessionValue(USER_ID), cartItem)
            .then(res => {
                this.generateAlert("success", "Success", "Item added to bag");
            })
            .catch(error => {
                console.error(error);
                /*if(error.response && error.response.status === 422) {
                    var errors = error.response.data.error.message;
                    Object.keys(errors).map((key, i) => {
                        return this.generateAlert("danger", "Whoa, something's not right!", errors[key]);
                    });
                }
                if(error.response && error.response.status === 500) {
                    this.generateAlert("danger", "Whoa, something's not right!", "Server Error");
                }*/
            });
    }

    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }

        this.setState({alerts: [...this.state.alerts, newAlert]});
    }

    renderErrors = () => {
        const {errors} = this.state;
        return Object.keys(errors).map((key, i) => {
            return (
                <Alert key={key} variant="danger">
                    <Alert.Heading>Something's not right</Alert.Heading>
                    <p>
                        {errors[key]}
                    </p>
                </Alert>
            )
        });
    }

    getColors = () => {
        const {productVariations} = this.state;
        let colors = productVariations.map(variation => variation.color)
            .reduce((acc, color) => {
                if (acc.indexOf(color) < 0)
                    acc.push(color);
                return acc;
            }, []);
        this.setState({colors: colors, selectedColor: colors[0]}, () => this.getSizes());
    }

    getSizes = () => {
        const {productVariations, selectedColor} = this.state;
        let sizes = productVariations.filter(variation =>
            variation.color === selectedColor).map(variation => variation.size);
        let selectedVariation = this.getVariationByColorAndSize(selectedColor, sizes[0]);
        let image = '';
        if (selectedVariation !== undefined) {
            image = selectedVariation.imageUrl;
        }
        this.setState({
            sizes: sizes,
            selectedSize: sizes[0],
            selectedVariation: selectedVariation,
            image: image,
            qty: 1
        });
    }

    getVariationByColorAndSize = (color, size) => {
        const {productVariations} = this.state;
        return productVariations.find(variation =>
            variation.color === color && variation.size === size
        );
    }
}

export default ProductDetails;
