import React from 'react';
import Card from "react-bootstrap/Card";
import ProductService from "../../services/ProductService";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingBag} from "@fortawesome/free-solid-svg-icons";
import Col from "react-bootstrap/Col";

const ProductionOrderRow = ({product}) => {
    let price = parseFloat(product.price).toFixed(2);
    let discountedPrice;
    if (product.discountPercent > 0) {
        var productService = new ProductService();
        discountedPrice = productService.discountedPrice(price, product.discountPercent);
    }

    return (
        <React.Fragment>
            <Col md={3} sm={6}>
                <div className="product-grid">
                    <div className="product-image">
                        <a href={"/products/" + product.productId}>
                            <img className="pic-1"
                                 src={product.imageUrl}/>
                        </a>
                        <ul className="social">
                            <li><a href={"/products/" + product.productId} data-tip="Add to Bag"><FontAwesomeIcon
                                icon={faShoppingBag}/></a></li>
                        </ul>
                        {product.discountPercent !== 0 ? (<><span className="product-sale-label">Sale</span>
                            <span className="product-discount-label">-{product.discountPercent}%</span></>) : <></>}
                    </div>
                    <div className="product-content">
                        <h3 className="title"><a href={"/products/" + product.productId}>{product.name}</a></h3>
                        <div className="price">
                            LKR {discountedPrice}
                            <span>LKR {price}</span>
                        </div>
                        <a className="add-to-cart" href={"/products/" + product.productId}>ADD TO BAG</a>
                    </div>
                </div>
            </Col>
        </React.Fragment>
    );
}

export default ProductionOrderRow;