import React, {Component} from 'react';
import {ClipLoader} from 'react-spinners';
import ProductService from "../../services/ProductService";
import ProductGridItem from "./ProductGridItem";
import Container from "react-bootstrap/Container";

class ProductsGrid extends Component {
    productService = new ProductService();

    constructor() {
        super();

        this.state = {
            products: [],
            isLoading: true
        }
    }

    componentDidMount() {
        document.title = "GiGi | Products";
        this.getAllProducts();
    }

    renderProducts() {
        const {products} = this.state;
        return products.map(product => {
            return (
                <React.Fragment key={product.productId}>
                    <ProductGridItem product={product}/>
                </React.Fragment>
            )
        })
    }

    render() {
        const {isLoading} = this.state;
        if (isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            )
        }

        return (
            <Container>
                <div>
                    <h3>All Products</h3>
                    <div class="row">
                        {this.renderProducts()}
                    </div>
                </div>
                <hr/>
            </Container>);
    }

    getAllProducts = () => {
        let categoryCode = this.props.match.params.categoryCode;
        let type = this.props.match.params.type;
        this.productService.getProductsByCategoryCodeAndType(categoryCode, type)
            .then(res => this.setState({products: res.data, isLoading: false}))
            .catch(error => {
                console.error(error);
                this.setState({isLoading: false});
            });
    }
}

export default ProductsGrid;


