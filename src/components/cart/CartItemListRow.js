import React, {Component} from 'react';
import {Link} from "react-router-dom";
import ProductService from "../../services/ProductService";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import Button from "react-bootstrap/Button";

class CartItemListRow extends Component {
    removeCartItem = (event) => {
        this.props.removeCartItem(this.props.cartItem);
    }

    render()
    {
        let cartItem = this.props.cartItem;
        let productService = new ProductService();
        let product = cartItem.productVariation.product;
        let price = parseFloat(product.price).toFixed(2);
        let discountedPrice = parseFloat(productService.discountedPrice(product.price, product.discountPercent));
        let totalPrice = parseFloat(discountedPrice * cartItem.quantity);

        return (
            <React.Fragment>
                <td>
                    <Row>
                        <Col md={3}>
                            <Image
                                src={cartItem.productVariation.imageUrl}
                                alt={product.name}
                                className="image-responsive"
                                width={"120px"}
                            />
                        </Col>
                        <Col md={8}>
                            <Row><Link to={"/products/" + product.productId}><h5>{product.name}</h5></Link></Row>
                            <Row>Product No. {product.productId}</Row><br/>
                            <Row><h6>Color: </h6> {cartItem.productVariation.color}</Row>
                            <Row><h6>Size: </h6> {cartItem.productVariation.size}</Row>
                            <Row><Button variant={"default"} onClick={this.removeCartItem}><FontAwesomeIcon icon={faTrashAlt}/> Remove</Button></Row>
                        </Col>
                    </Row>

                </td>
                <td>{cartItem.quantity}</td>
                <td>
                    <Row>LKR {discountedPrice}</Row>
                    <Row><strike>LKR {price}</strike></Row>
                </td>
                <td className={"text-left"}>LKR {totalPrice}</td>
            </React.Fragment>
        );
    }
}

export default CartItemListRow;