import React, {Component} from 'react';
import {ClipLoader} from 'react-spinners';
import CartService from "../../services/CartService";
import CartItemListRow from "./CartItemListRow";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class CartItemList extends Component {
    cartService = new CartService();

    constructor() {
        super();

        this.state = {
            cartItems: [],
            isLoading: true
        }
    }

    componentDidMount() {
        document.title = "GiGi | Shopping Bag";
        this.getAllCartItems();
    }

    renderCartItems() {
        const {cartItems} = this.state;
        return cartItems.map(item => {
            return (
                <tr key={item.cartItemId}>
                    <CartItemListRow cartItem={item} removeCartItem={this.removeCartItem}/>
                </tr>
            )
        })
    }

    render() {
        const {isLoading} = this.state;
        if (isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            )
        }

        return (
            <div className={"container"}>
                <Row>
                    <Col md={9}>
                        <h2>Shopping Bag</h2>
                    </Col>
                    <Col md={3}><Button block onClick={this.redirectCheckout}>Checkout</Button></Col>
                </Row>
                <br/>
                <Table>
                    <thead>
                    <tr>
                        <th scope="col">Item</th>
                        <th scope="col">Qty</th>
                        <th scope="col">Price</th>
                        <th scope="col">Total Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderCartItems()}
                    <tr><td/><td/><td><h5>Order Total</h5></td><td><h6>LKR {this.cartService.getTotalPrice(this.state.cartItems)}</h6></td></tr>
                    </tbody>
                </Table>
            </div>);
    }

    getAllCartItems = () => {
        this.cartService.getCartItems(getSessionValue(USER_ID))
            .then(res => this.setState({cartItems: res.data, isLoading: false}))
            .catch(error => {
                console.error(error);
                this.setState({isLoading: false});
            });
    }

    removeCartItem = (cartItem) => {
        let newCartItems = [...this.state.cartItems];
        let index = newCartItems.indexOf(cartItem);

        this.cartService.removeCartItem(getSessionValue(USER_ID), cartItem.cartItemId)
            .then(res => {
                if (index !== -1) {
                    newCartItems.splice(index, 1)
                    this.setState({cartItems: newCartItems})
                }
            })
            .catch(error => {
                console.error(error);
            });
    }

    redirectCheckout = () => {
        if(this.state.cartItems.length === 0) {
            alert("No items in the cart");
        } else {
            this.props.history.push("/checkout");
        }
    }
}

export default CartItemList;


