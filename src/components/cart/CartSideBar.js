import React, {Component} from 'react';
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import Col from "react-bootstrap/Col";
import ProductService from "../../services/ProductService";
import Row from "react-bootstrap/Row";
import CartService from "../../services/CartService";

class CartSideBar extends Component {
    productService = new ProductService();
    cartService = new CartService();

    componentDidMount() {

    }

    renderCartItem = () => {
        let {cartItems} = this.props;
        return cartItems.map(item => {
            let product = item.productVariation.product;
            return (
                <ListGroupItem key={item.cartItemId} className="d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 className="my-0">{product.name}</h6>
                        <small className="text-muted"><b>Color: </b>{item.productVariation.color}</small>
                        <br/>
                        <small className="text-muted"><b>Size: </b>{item.productVariation.size}</small>
                        <br/>
                        <small className="text-muted"><b>Quantity: </b>{item.quantity}</small>
                    </div>
                    <span
                        className="text-muted"><Row>LKR {((this.productService.discountedPrice(product.price, product.discountPercent)) * item.quantity).toFixed(2)}</Row>
                        <Row><strike>LKR {(product.price * item.quantity).toFixed(2)}</strike></Row></span>
                </ListGroupItem>
            )
        })
    }

    render() {
        return (
            <Col md={5} mb={4} className="order-md-2">
                <h4 className="d-flex justify-content-between align-items-center mb-3">
                    <span className="text-muted">Your cart</span>
                    <span className="badge badge-secondary badge-pill">{this.props.cartItems.length}</span>
                </h4>
                <ListGroup className="mb-3">
                    {this.renderCartItem()}
                    <ListGroupItem className="d-flex justify-content-between">
                        <span>Total (LKR)</span>
                        <strong>Rs {this.cartService.getTotalPrice(this.props.cartItems)}</strong>
                    </ListGroupItem>
                </ListGroup>
            </Col>
        );
    }
}

export default CartSideBar;